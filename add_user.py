#!/usr/bin/env python

import chain
import sys

username = raw_input("[Username]: ")
fi = open(chain.options.passwd, "r")
contents = fi.read()
for line in contents.rsplit('\n\n'):
	if username in line:
		print "user already exists"
		sys.exit(0)
fi.close()

password = raw_input("[Password]: ")



m_hash, nonce = chain.Service.make_hash(password)

if m_hash is None:
	print "You must first initialize the array"
	sys.exit(0)

print 
print "Username:", username
print "Hash:", m_hash

fi = open(chain.options.passwd, "a")
fi.write(username+":"+nonce+":"+m_hash+"\n\n")
fi.close()

