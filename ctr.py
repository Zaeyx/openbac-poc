from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Util import Counter

def get_nonce():
	return get_random_bytes(8)

def encrypt(key, nonce, plaintext):
	e = AES.new(key, AES.MODE_CTR, counter=Counter.new(64, prefix=nonce))
	return e.encrypt(plaintext)

def decrypt(key, nonce, ciphertext):
	d = AES.new(key, AES.MODE_CTR, counter=Counter.new(64, prefix=nonce))
	return d.decrypt(ciphertext)


