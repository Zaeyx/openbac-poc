"""
Small Little POC For Ball And Chain

Plenty of things have to change before this code is
Cryptographically safe. (See, concat of ":::" gives signatures for True.)
			(See, small Array)
			(See, No md5_crypt())
			(See, Full Range of data not in array [Signatures])
			(Etc...)


But this is an example of how this idea could function.

Requires PyCrypto.

Have fun
"""


from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Util import Counter

import base64
import os
import random
import hashlib
import time

class options:
	"""Chain options and universal functions"""
	
	#OPTIONS
	debug = True
	passes = 30
	fi_name = "gold.chain"
	passwd = 'passwd'
	#ENDOPTIONS

	@staticmethod
	def is_chain():
		return True

	@staticmethod
	def is_field():
		#No Randomness check or anything, just length
		fi = open(options.fi_name,"r")
		data = fi.read()
		if(len(data)/32 == options.passes):
			return True
		else:
			return False

	@staticmethod
	def make_field():
		if(options.is_field()):
			ans = raw_input("Overwrite previous array?[y/n]: ")
			if ans.lower() != 'y':
				print "Quitting"
				return False
		a = ''
		for i in xrange(options.passes):
			a = a + Service.md5_hash(str(random.randint(1,4296967296)+time.time())) + "\n"
		fi = open(options.fi_name,"w")
		fi.write(a)
		fi.close()
		
		fi = open(options.fi_name,"r")
		b = fi.read()
		if(a == b):
			print "Done"
		else:
			print "Something seems to have gone wrong."
			

class Service:
	"""Services to be called for ADT"""
	@staticmethod
	def pad_add(num):
		num = str(hex(num))[2:]
		passes = hex(options.passes)[2:]
		while len(num) < len(passes):
			num = '0' + num
		return num

	@staticmethod
	def md5_hash(password):
		h = hashlib.md5()
		h.update(str(password))
		return h.hexdigest()
	
	@staticmethod
	def make_hash(password):
		nonce = get_random_bytes(8)
		if not options.is_field():
			return None

		cipher = AES.new(Service.md5_hash(password), AES.MODE_CTR, counter=Counter.new(64, prefix=nonce))
		num = random.randint(1, options.passes)
		contents = open(options.fi_name, "r")
		contents.seek(num*33,0)
		data = contents.readline()[:-1]
		out = cipher.encrypt(Service.pad_add(num).decode("hex") + data.decode("hex"))
		if options.debug:
			print "\n ***Debug Data*** "
			print "Data, len: ", data, len(data), 
			print "Address: ", num
			print "Binary Output: ", out
		return out.encode("hex"), nonce.encode("hex")

	@staticmethod
	def decrypt(password, nonce, data):
		cipher = AES.new(Service.md5_hash(password),AES.MODE_CTR, counter=Counter.new(64, prefix=nonce.decode("hex")))
		return cipher.decrypt(data.decode("hex"))

	@staticmethod
	def check_pass(decode):
		if not options.is_field():
			return False

		contents = open(options.fi_name, "r")
		try:
			num = decode.encode("hex")[:len(hex(options.passes)[2:])]
			data = decode.encode("hex")[len(hex(options.passes)[2:]):]
			num = int('0x' + num, 16)
			if options.debug:
				print "Address: ", num
				print "Content, len: ",  data, len(data)
		except:
			return False
		
		contents.seek(int(num)*33,0)
		data_0 = contents.readline()[:-1]
		if data_0 == data:
			return True
		else:
			return False
		


class Crypt:
	"""Chain Crypto Methods"""

	BLOCK_SIZE = 32
	PADDING = '{'

	@staticmethod
	def pad(s): 
		return s + (Crypt.BLOCK_SIZE - len(s) % Crypt.BLOCK_SIZE) *Crypt. PADDING

	@staticmethod
	def EncodeAES(c, s):
		return base64.b64encode(c.encrypt(Crypt.pad(s)))

	@staticmethod
	def DecodeAES(c, e):
		return c.decrypt(base64.b64decode(e)).rstrip(Crypt.PADDING)
		
	
