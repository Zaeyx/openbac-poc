#!/usr/bin/env python

import chain

username = raw_input("Enter Username: ")
password = raw_input("Enter Password: ")
m_hash = None

fi = open(chain.options.passwd, 'r')
data = fi.read()
for line in data.split('\n'):
	if username in line:
		print line
		nonce = line.rsplit(":")[1]
		m_hash = line.rsplit(":")[2]
		
auth = False
if m_hash is not None:
	a = chain.Service.decrypt(password,nonce,m_hash)
	print
	print "Decrypted Data:", a.encode("hex")
	print "Decrypted Data Length:", len(a.encode("hex"))
	auth = chain.Service.check_pass(chain.Service.decrypt(password,nonce,m_hash))

print "Access Allowed:", auth

